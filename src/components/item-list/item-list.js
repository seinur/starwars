import React, { Component } from 'react';

import './item-list.css';

export default class ItemList extends Component {

  state = {
    items: null
  }

  componentDidMount = () => {
    this.updatePersonList()
  }

  updatePersonList = () => {
    const {getData} = this.props;

    getData().then((data) => {
      this.setState({items: data})
    })
  }

  renderItem = (arr) => {
    return arr.map((elem) => {
      return (<li onClick={() => {this.props.onItemSelected(elem.id)}} key={elem.id} className="list-group-item">
        {elem.name}
      </li>)
    })
  }

  render() {
    const items = this.state.items;

    if (!items) {
      return (<p>Loading...</p>)
    }

    const edited_items = this.renderItem(items)

    return (
      <ul className="item-list list-group">
        {edited_items}
      </ul>
    );
  }
}
