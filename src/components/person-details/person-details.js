import React, { Component } from 'react';

import './person-details.css';
import Spinner from '../spinner';

export default class PersonDetails extends Component {

  state = {
    item: null
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.personId !== this.props.personId) {
        this.updatePerson()
    }
  }

  updatePerson = () => {
    const {personId} = this.props;
    const {getData} = this.props;

    getData(personId).then((data) => {
      this.setState({item: data})
    })
  }

  render() {
    const {item} = this.state;

    if (!item) {
      return (<Spinner />)
    }

    const {item: {id, name, gender, birthYear, eyeColor}} = this.state;
    const url = `https://starwars-visualguide.com/assets/img/characters/${this.props.personId}.jpg`

    console.log(url);

    return (
      <div className="person-details card">
        <img className="person-image"
          src={url} />

        <div className="card-body">
          <h4>{name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Gender</span>
              <span>{gender}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Birth Year</span>
              <span>{birthYear}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Eye Color</span>
              <span>{eyeColor}</span>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}
